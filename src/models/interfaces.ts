export interface i_Clientes {
    id: string;
    identificacion: number;
    nombres: string;
    apellidos: string;
    correo: string;
    direccion: string;
    telefono: string;
    estado: string;
    creacion: Date;
    modificacion: Date;
    plazo: number;
}

export interface i_Productos {
    id: string;
    costo: number;
    descripcion: string;
    existencias: number;
    iva: number;
    precio: number;
}

export interface i_Ventas {
    id?: string;
    cliente: any;
    productos: any[];
    totalVenta: number;
    saldo: number;
    estado: string;
    metodoPago: any;
    cambio: number;
    debe: number;
    creacion: Date;
    anulacion: Date;
}