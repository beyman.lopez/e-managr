import { Component, OnInit } from "@angular/core";
import { HomeComponent } from 'src/app/home/components/home/home.component';
import { AuthService } from 'src/services/auth/auth.service';

@Component({
    selector: "app-menu-app",
    templateUrl: "./menu-app.component.html",
    styleUrls: ["./menu-app.component.scss"]
})
export class MenuAppComponent implements OnInit {
    v__darkTheme: boolean = false;

    constructor(private home: HomeComponent, private auth: AuthService) {
        console.log(this.v__darkTheme);
        if (!localStorage.getItem("darkTheme")) {
            localStorage.setItem("darkTheme", "false");
        }
        if (localStorage.getItem("darkTheme") === "true") {
            this.v__darkTheme = true;
        } else {
            this.v__darkTheme = false;
        }
    }

    ngOnInit() { }

    toggleTheme() {
        if (this.v__darkTheme) {
            localStorage.setItem("darkTheme", "true");
            document.body.classList.add("dark");
            this.v__darkTheme = true;
        } else {
            localStorage.setItem("darkTheme", "false");
            document.body.classList.remove("dark");
            this.v__darkTheme = false;
        }
        if (this.auth.isLogged) {
            this.home.initChart();
        }
    }
}
