import { Component, Inject } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
    selector: 'snack',
    template: ``,
})
export class Utils {
    constructor(
        private toastController: ToastController,
    ) { }
    /**
	* @author Beyman
	* @param {String} tipo (recibe solo dos parametros 'c' correcto, 'e' error y 'a' alerta)
    * @param {String} mensaje ('Hola soy un toast')
    + @param {number} duracion (milisegundos ejemplo 3000 = 3 segundos)
	* @description Abrir un toast de tipo error o correcto
	*/
    public async openToast(tipo: string, mensaje: string, duracion: number = 3000) {
        let color;
        if (tipo === 'c') {
            color = 'success';
        }
        if (tipo === 'e') {
            color = 'danger';
        }
        if (tipo === 'a') {
            color = 'warning';
        }
        const toast = await this.toastController.create({
            showCloseButton: true,
            closeButtonText: 'Cerrar',
            message: mensaje,
            duration: duracion,
            color: color,
        });
        toast.present();
    }

}