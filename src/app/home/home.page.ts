import { Component, OnInit } from "@angular/core";
import { ActivationStart, Router, ActivatedRoute } from "@angular/router";
import { MenuAppComponent } from "../menu/menu-app/menu-app.component";
import { PopoverController, NavController } from "@ionic/angular";
import { HomeComponent } from "./components/home/home.component";
import { SalesComponent } from "./components/sales/sales.component";
import { UserComponent } from "./components/user/user.component";

@Component({
    selector: "app-home",
    templateUrl: "home.page.html",
    styleUrls: ["home.page.scss"]
})
export class HomePage implements OnInit {
    routeTitle: string = "Inicio";
    icon: string = "home"
    showHeader: boolean = true;
    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        public popoverController: PopoverController,
        private home: HomeComponent,
        private sales: SalesComponent,
        private user: UserComponent,
        private navCtlr: NavController
    ) { }

    ngOnInit() {
        // console.log(this.activeRoute);

        this.router.events.subscribe(event => {
            if (event instanceof ActivationStart) {
                if (event.snapshot.data.title) {
                    console.log(event.snapshot.data.title);
                    this.routeTitle = event.snapshot.data.title;
                    // this.showHeader = event.snapshot.data.showHeader;

                    if (this.routeTitle === "Inicio") {
                        this.home.logScrollStart();
                        this.icon = "home";
                    }

                    if (this.routeTitle === "Ventas") {
                        this.sales.logScrollStart();
                        this.icon = "local_mall";
                    }

                    if (this.routeTitle === "Usuario") {
                        this.user.logScrollStart();
                        this.icon = "person";
                    }
                }
            }
        });
    }

    async presentPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: MenuAppComponent,
            event: ev,
            translucent: true,
            cssClass: "app-menu"
        });

        return await popover.present();
    }

    hideTabBar() {
        document.getElementById("round_tabs").classList.add("hide");
    }

    showTabBar() {
        document.getElementById("round_tabs").classList.remove("hide");
    }
}
