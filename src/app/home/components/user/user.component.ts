import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";
import { AuthService } from 'src/services/auth/auth.service';

@Component({
    selector: "app-user",
    templateUrl: "./user.component.html",
    styleUrls: ["./user.component.scss"]
})
export class UserComponent implements OnInit {
    v__scrollTop: number = 0;
    constructor(
        public nav: NavController,
        private auth: AuthService,
    ) { }

    ngOnInit() { }

    public logScrollStart() {
        console.log(Number(localStorage.getItem("userScroll")));
        document.getElementById("appTitle").classList.remove("hide");
        if (Number(localStorage.getItem("userScroll")) > 1) {
            document.getElementById("appTitle").classList.add("normal");
            document.getElementById("buttonMore").classList.add("normal");
        } else {
            document.getElementById("appTitle").classList.remove("normal");
            document.getElementById("buttonMore").classList.remove("normal");
        }
    }

    logScrolling(p__event) {
        // console.log(p__event);
        this.v__scrollTop = p__event.detail.currentY;
        localStorage.setItem("userScroll", this.v__scrollTop.toString());

        if (p__event.detail.currentY > 1) {
            document.getElementById("appTitle").classList.add("normal");
            document.getElementById("buttonMore").classList.add("normal");
        } else {
            document.getElementById("appTitle").classList.remove("normal");
            document.getElementById("buttonMore").classList.remove("normal");
        }
    }

    logScrollEnd() { }

    logOut() {
        localStorage.removeItem("userScroll");
        localStorage.removeItem("homeScroll");
        localStorage.removeItem("salesScroll");
        this.nav.navigateRoot("auth");
        this.auth.onLogOut();
    }
}
