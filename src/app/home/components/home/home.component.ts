import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { GetsService } from "src/services/gets/gets.service";
import * as moment from "moment";
import { Chart } from "chart.js";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
    @ViewChild("barCanvasFechas", { static: true }) barCanvasFechas: ElementRef;
    @ViewChild("barCanvasClientes", { static: true })
    barCanvasClientes: ElementRef;

    private barChartFechas: Chart;
    private barChartClientes: Chart;

    v__ventasDatabase: Array<any> = [];
    v__fechas: Array<string> = [];
    v__totalesFechas: Array<number> = [];

    v__clientes: Array<string> = [];
    v__totalesClientes: Array<number> = [];
    v__scrollTop: number = 0

    constructor(private cr__service: GetsService) { }

    async ngOnInit() {
        // ventas
        await this.cr__service.obtenerVentas().subscribe(async p__ventas => {
            this.v__fechas = [];
            this.v__totalesFechas = [];

            this.v__clientes = [];
            this.v__totalesClientes = [];

            console.log(p__ventas);
            this.v__ventasDatabase = p__ventas;
            // console.log(this.v__ventasDatabase);
            localStorage.setItem(
                "ventas",
                JSON.stringify(this.v__ventasDatabase)
            );

            this.v__ventasDatabase = JSON.parse(localStorage.getItem("ventas"));
            console.log(this.v__ventasDatabase);

            let l__fechas = [];
            let l__chartVentasFechas = [];

            let l__clientes = [];
            let l__chartVentasClientes = [];

            if (this.v__ventasDatabase != undefined) {
                this.v__ventasDatabase.forEach(p__venta => {
                    l__fechas.push(
                        moment(
                            new Date(p__venta.creacion.seconds * 1000)
                        ).format("DD-MM-YYYY")
                    );

                    l__clientes.push(
                        `${p__venta.cliente.identificacion}-ID-${p__venta.cliente.nombres} ${p__venta.cliente.apellidos}`
                    );
                });

                console.log(Array.from(new Set(l__fechas)));

                let fechasunicas = Array.from(new Set(l__fechas));
                let clientesunicos = Array.from(new Set(l__clientes));

                await fechasunicas.forEach(fecha => {
                    let l__total = 0;
                    this.v__ventasDatabase.forEach(p__venta => {
                        if (
                            moment(
                                new Date(p__venta.creacion.seconds * 1000)
                            ).format("DD-MM-YYYY") === fecha
                        ) {
                            l__total = l__total + p__venta.totalVenta;
                            fecha = moment(
                                new Date(p__venta.creacion.seconds * 1000)
                            ).format("DD-MM-YYYY");
                        }
                    });

                    l__chartVentasFechas.push({
                        fecha: fecha,
                        total: l__total
                    });

                    fecha = "";
                    l__total = 0;
                });

                console.log(l__chartVentasFechas);

                await l__chartVentasFechas.forEach(p__venta => {
                    this.v__fechas.push(p__venta.fecha);
                    this.v__totalesFechas.push(p__venta.total);
                });

                console.log(this.v__fechas);
                console.log(this.v__totalesFechas);

                await clientesunicos.forEach(p__cliente => {
                    let l__total = 0;
                    this.v__ventasDatabase.forEach(p__venta => {
                        let l__consulta = `${p__venta.cliente.identificacion}-ID-${p__venta.cliente.nombres} ${p__venta.cliente.apellidos}`;
                        if (l__consulta === p__cliente) {
                            l__total = l__total + p__venta.totalVenta;
                        }
                    });

                    l__chartVentasClientes.push({
                        cliente: p__cliente.split("-ID-")[1],
                        total: l__total
                    });

                    l__total = 0;
                });

                console.log(l__chartVentasClientes);

                await l__chartVentasClientes.forEach(p__venta => {
                    this.v__clientes.push(p__venta.cliente);
                    this.v__totalesClientes.push(p__venta.total);
                });

                console.log(this.v__clientes);
                console.log(this.v__totalesClientes);

                await this.initChart();
            }
        });
    }

    public logScrollStart() {
        console.log(Number(localStorage.getItem("homeScroll")));
        document.getElementById("appTitle").classList.remove("hide");
        if (Number(localStorage.getItem("homeScroll")) > 1) {
            document.getElementById("appTitle").classList.add("normal");
            document.getElementById("buttonMore").classList.add("normal");
        } else {
            document.getElementById("appTitle").classList.remove("normal");
            document.getElementById("buttonMore").classList.remove("normal");
        }
    }

    logScrolling(p__event) {
        // console.log(p__event);
        this.v__scrollTop = p__event.detail.currentY;
        localStorage.setItem("homeScroll", this.v__scrollTop.toString())

        if (p__event.detail.currentY > 1) {
            document.getElementById("appTitle").classList.add("normal");
            document.getElementById("buttonMore").classList.add("normal");
        } else {
            document.getElementById("appTitle").classList.remove("normal");
            document.getElementById("buttonMore").classList.remove("normal");
        }
    }

    logScrollEnd() { }


    initChart() {
        this.barChartFechas = new Chart(this.barCanvasFechas.nativeElement, {
            type: "line",
            data: {
                labels: this.v__fechas,
                datasets: [
                    {
                        label: "Total vendido por fechas",
                        data: this.v__totalesFechas,
                        borderColor: 'rgba(162, 163, 165, 1)',
                        borderWidth: 5,
                        fill: false,
                        backgroundColor: [
                            "rgba(54, 162, 235, 1)",
                            "rgba(255, 99, 132, 1)",
                            "rgba(255, 206, 86, 1)",
                            "rgba(75, 192, 192, 1)",
                            "rgba(153, 102, 255, 1)",
                            "rgba(255, 159, 64, 1)"
                        ],
                        radius: 12,
                        pointHoverRadius: 16,
                        pointBorderWidth: 0,
                        pointBorderColor: 'rgba(0,0,0,0)',
                        lineTension: 0,
                    }
                ]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true
                            }
                        }
                    ]
                }
            }
        });

        this.barChartClientes = new Chart(
            this.barCanvasClientes.nativeElement,
            {
                type: "line",
                data: {
                    labels: this.v__clientes,
                    datasets: [
                        {
                            label: "Total vendido al cliente",
                            data: this.v__totalesClientes,
                            borderColor: 'rgba(162, 163, 165, 1)',
                            borderWidth: 5,
                            fill: false,
                            backgroundColor: [
                                "rgba(54, 162, 235, 1)",
                                "rgba(255, 99, 132, 1)",
                                "rgba(255, 206, 86, 1)",
                                "rgba(75, 192, 192, 1)",
                                "rgba(153, 102, 255, 1)",
                                "rgba(255, 159, 64, 1)"
                            ],
                            radius: 12,
                            pointHoverRadius: 16,
                            pointBorderWidth: 0,
                            pointBorderColor: 'rgba(0,0,0,0)',
                            lineTension: 0,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                    beginAtZero: true
                                }
                            }
                        ]
                    }
                }
            }
        );
    }

    getContent() {
        return document.querySelector("#home");
    }
    scrollToTop() {
        console.log(this.getContent().scrollTop);
    }
}
