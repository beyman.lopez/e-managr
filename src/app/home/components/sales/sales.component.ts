import { Component, OnInit, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith, map } from "rxjs/operators";
import { GetsService } from "src/services/gets/gets.service";
import { ToastController, PopoverController } from "@ionic/angular";
import { i_Ventas } from "../../../../models/interfaces";
import { MatAutocompleteTrigger } from "@angular/material";
import { MenuSalesComponent } from './menu-sales/menu-sales/menu-sales.component';
import { PrintService } from 'src/services/print/print.service';

export interface IN__PRODUCTO {
	cantidad: number;
	isEditing: boolean;
	totalItem: number,
	producto: any;
}

@Component({
	selector: "app-sales",
	templateUrl: "./sales.component.html",
	styleUrls: ["./sales.component.scss"]
})
export class SalesComponent implements OnInit {
	v__productosDatabase: Array<any> = [];
	v__clientesDatabase: Array<any> = [];
	v__ventasDatabase: Array<any> = [];

	v__listaProductosVender: Array<IN__PRODUCTO> = [];

	v__filterAutocomplete: any[];
	// v__filterAutocompleteOptions: Observable<string[]>;
	// f__filterControl = new FormControl();

	v__clientesAutocomplete: any[];
	v__clientesAutocompleteOptions: Observable<string[]>;
	f__clientesControl = new FormControl();

	v__cantidad: number = 0;
	v__precio: number = 0;

	v__descuentoPorcentaje: number = 0;
	v__descuentoValor: number = 0;
	v__valorEfectivo: number = 0;
	v__valorTransferencia: number = 0;

	v__totalVenta: number = 0;
	v__totalPagar: number = 0;

	v__clienteSeleccionado: any = {};
	v__nombreCliente = "";

	v__mostrarBusquedaCliente: boolean = false;

	ng__Efectivo: number = 0;
	v__Efectivo: number = 0;
	ng__Transferencia: number = 0;
	v__Cambio: number = 0;
	v__Debe: number = 0;

	v__idFactura: string = "000";

	v__scrollTop: number = 0;

	@ViewChild("html__filtroProductos", {
		read: MatAutocompleteTrigger,
		static: true
	})
	autocompleteProd: MatAutocompleteTrigger;
	@ViewChild("html__filtroClientes", {
		read: MatAutocompleteTrigger,
		static: true
	})
	autocompleteCliet: MatAutocompleteTrigger;

	bluetoothList: any = [];
	selectedPrinter: any;

	constructor(
		private cr__service: GetsService,
		public toastController: ToastController,
		public popoverController: PopoverController,
		private print: PrintService,
	) { }

	ngOnInit() {
		this.getData();
	}

	public v__searchOptions: any[] = [];
	public v__loadedsearchOptions: any[] = [];

	initializeItems(): void {
		this.v__searchOptions = this.v__loadedsearchOptions;
	}
	v__searchProduct: string = ""

	showAllItems() {
		this.v__searchOptions = this.v__loadedsearchOptions;
	}

	removeAllElemets() {
		if (!this.v__searchProduct) {
			setTimeout(() => {
				this.v__searchOptions = [];
			}, 50);
		}
	}
	selectOption(evt) {
		console.log(evt.target.id);

		let selected = this.v__searchOptions.filter(option => {
			if (evt.target.id) {
				if (`${option.id}`.toLowerCase().indexOf(`${evt.target.id}`.toLowerCase()) > -1) {
					return true;
				}
				return false;
			}
		});

		console.log(selected);

		this.addToList(selected[0])


		this.v__searchOptions = []
		this.v__searchProduct = ""
	}

	filterList(evt) {
		console.log(evt.srcElement.value);

		this.initializeItems();
		const searchTerm = evt.srcElement.value;

		if (!searchTerm) {
			// this.v__searchOptions = []
			return;
		}

		this.v__searchOptions = this.v__searchOptions.filter(option => {
			if (option.id && searchTerm) {
				if (`${option.id} ${option.descripcion}`.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
					return true;
				}
				return false;
			}
		});
	}

	public logScrollStart() {
		console.log(Number(localStorage.getItem("salesScroll")));
		document.getElementById("appTitle").classList.remove("normal");
		document.getElementById("buttonMore").classList.remove("normal");
		if (Number(localStorage.getItem("salesScroll")) > 1) {
			document.getElementById("appTitle").classList.add("hide");
			document.getElementById("buttonMore").classList.add("hide");
		} else {
			document.getElementById("appTitle").classList.remove("hide");
			document.getElementById("buttonMore").classList.remove("hide");
		}
	}

	logScrolling(p__event) {
		// console.log(p__event);
		this.v__scrollTop = p__event.detail.currentY;
		localStorage.setItem("salesScroll", this.v__scrollTop.toString());
		if (p__event.detail.currentY > 1) {
			document.getElementById("appTitle").classList.add("hide");
			document.getElementById("buttonMore").classList.add("hide");
			document.getElementById("browseInput").classList.add("top");
		} else {
			document.getElementById("appTitle").classList.remove("hide");
			document.getElementById("buttonMore").classList.remove("hide");
			document.getElementById("browseInput").classList.remove("top");
		}
		this.autocompleteCliet.panelOpen
			? this.autocompleteCliet.updatePosition()
			: null;
		// this.autocompleteProd.panelOpen
		// 	? this.autocompleteProd.updatePosition()
		// 	: null;
	}

	logScrollEnd() { }

	getContent() {
		return document.querySelector("#sales");
	}

	scrollToTop() {
		console.log(this.getContent().scrollTop);
	}

	toggleTabBar() {
		document.getElementById("round_tabs").classList.toggle("hide");
	}

	async getData() {
		// productos
		await this.cr__service.obtenerProductos().subscribe(async p__productos => {
			// console.log(p__productos);
			this.v__productosDatabase = p__productos;
			// this.initFilterProducts();
			this.v__loadedsearchOptions = [];
			await this.v__productosDatabase.forEach(p__producto => {
				this.v__loadedsearchOptions.push({
					valorFiltro: `${p__producto.id} ${p__producto.descripcion}`,
					...p__producto
				});
			});
		});

		// clientes
		this.cr__service.obtenerClientes().subscribe(p__clientes => {
			// console.log(p__clientes);
			this.v__clientesDatabase = p__clientes;
			this.initFilterClientes();
		});

		// ventas
		this.cr__service.obtenerVentas().subscribe(p__ventas => {
			console.log(p__ventas);
			this.v__ventasDatabase = p__ventas;
			console.log(this.v__ventasDatabase);

			let ultimaVenta;
			let ventasIds = [];

			if (this.v__ventasDatabase != undefined) {
				this.v__ventasDatabase.filter(p__venta => {
					console.log(p__venta.creacion.toDate());
					ventasIds.push(Number(p__venta.id));
				});
				// console.log(ventasIds);

				ultimaVenta = Math.max(...ventasIds);
				// console.log(ultimaVenta);
				this.v__idFactura = `00${ultimaVenta}`;
				console.log(this.v__idFactura);
			} else {
				this.v__idFactura = "000";
			}

			localStorage.setItem(
				"ventas",
				JSON.stringify(this.v__ventasDatabase)
			);
		});
	}

	// async initFilterProducts() {
	// 	this.v__filterAutocomplete = [];
	// 	await this.v__productosDatabase.forEach(p__producto => {
	// 		this.v__filterAutocomplete.push({
	// 			valorFiltro: `${p__producto.id} ${p__producto.descripcion}`,
	// 			...p__producto
	// 		});
	// 	});

	// 	this.v__filterAutocompleteOptions = await this.f__filterControl.valueChanges.pipe(
	// 		startWith(""),
	// 		map(value => this._filterProducts(value))
	// 	);
	// 	this.v__loadedsearchOptions = this.v__filterAutocomplete
	// 	console.log(this.v__filterAutocomplete);
	// }

	// displayFn(p__producto: any): string {
	// 	return p__producto ? p__producto.descripcion : undefined;
	// }

	// private _filterProducts(value: string): string[] {
	// 	const c__filterValue = value.toString().toLowerCase();
	// 	// console.log(c__filterValue);
	// 	return this.v__filterAutocomplete.filter(p__producto =>
	// 		p__producto.valorFiltro.toLowerCase().includes(c__filterValue)
	// 	);
	// }

	async initFilterClientes() {
		const c__clienteDefault = this.v__clientesDatabase.filter(
			p__cliente => p__cliente.id === "01"
		);

		console.log(c__clienteDefault);

		this.v__clienteSeleccionado = c__clienteDefault[0];
		this.v__nombreCliente = `${this.v__clienteSeleccionado.nombres} ${this.v__clienteSeleccionado.apellidos}`;

		this.v__clientesAutocomplete = [];
		await this.v__clientesDatabase.forEach(p__cliente => {
			this.v__clientesAutocomplete.push({
				valorFiltro: `${p__cliente.identificacion} ${p__cliente.nombres} ${p__cliente.apellidos}`,
				...p__cliente
			});
		});

		this.v__clientesAutocompleteOptions = await this.f__clientesControl.valueChanges.pipe(
			startWith(""),
			map(value => this._filterClientes(value))
		);

		console.log(this.v__clientesAutocomplete);
	}

	displayFnClientes(p__cliente: any): string {
		return p__cliente
			? `${p__cliente.nombres} ${p__cliente.apellidos}`
			: undefined;
	}

	private _filterClientes(value: string): string[] {
		const c__filterValue = value.toString().toLowerCase();
		// console.log(c__filterValue);
		return this.v__clientesAutocomplete.filter(p__cliente =>
			p__cliente.valorFiltro.toLowerCase().includes(c__filterValue)
		);
	}

	seleccionarCliente() {
		this.v__clienteSeleccionado = this.f__clientesControl.value;
		this.v__nombreCliente = `${this.v__clienteSeleccionado.nombres} ${this.v__clienteSeleccionado.apellidos}`;
		this.toggleCliente();
		this.f__clientesControl.patchValue("");
	}

	toggleCliente() {
		this.v__mostrarBusquedaCliente
			? (this.v__mostrarBusquedaCliente = false)
			: (this.v__mostrarBusquedaCliente = true);
	}

	async addToList(value) {
		let l__nuevoProducto = value;
		let l__existe = this.v__listaProductosVender.filter(
			p__producto => p__producto.producto.id === l__nuevoProducto.id
		);
		if (l__existe.length == 0) {
			l__nuevoProducto.existencias = l__nuevoProducto.existencias - 1;
			let l__productoVender = {
				cantidad: 1,
				isEditing: false,
				totalItem: l__nuevoProducto.precio,
				producto: l__nuevoProducto
			};
			await this.v__listaProductosVender.push(l__productoVender);
			let l__idItem = this.v__listaProductosVender.length - 1
			setTimeout(() => {
				document.getElementById(l__idItem.toString()).classList.remove("hidden")
			}, 10);
			console.log(this.v__listaProductosVender);
			// this.f__filterControl.patchValue("");
			this.calcularTotal();
		} else {
			this.productoExistente(l__nuevoProducto);
		}
		this.calcularTotal();
	}

	async productoExistente(p__producto) {

		console.log(p__producto);

		const c__index = this.v__listaProductosVender.findIndex(
			p__productoFilter =>
				p__productoFilter.producto.id === p__producto.id
		);
		let l__nuevaCantidad;
		if (this.v__listaProductosVender[c__index].isEditing) {

		} else {
			l__nuevaCantidad =
				this.v__listaProductosVender[c__index].cantidad + 1;
			const toast = await this.toastController.create({
				message: `Agregado ${p__producto.descripcion} + ${l__nuevaCantidad}`,
				duration: 2000,
				buttons: [
					{
						text: "Okay",
						role: "cancel",
						handler: () => {
							toast.dismiss();
						}
					}
				]
			});
			toast.present();
		}

		let l__precioProducto = this.v__listaProductosVender[c__index].producto
			.precio;

		this.v__listaProductosVender[c__index].cantidad = l__nuevaCantidad;
		this.v__listaProductosVender[c__index].totalItem =
			l__precioProducto * l__nuevaCantidad;

		// this.f__filterControl.patchValue("");
		this.calcularTotal();
	}

	deleteProduct(p__producto, p_index, slidingItem) {
		console.log(p__producto);
		console.log(p_index);
		slidingItem.close();
		document.getElementById(p_index).classList.add("hidden")
		setTimeout(() => {
			this.v__listaProductosVender.splice(p_index, 1)
			this.calcularTotal();
		}, 400);
	}

	editProduct(p__producto, p_index, slidingItem) {
		console.log(p__producto);
		console.log((p_index));
		slidingItem.close();
	}

	async presentPopover(ev: any, p__producto, p_index, slidingItem) {
		const popover = await this.popoverController.create({
			component: MenuSalesComponent,
			event: ev,
			translucent: true,
			cssClass: "app-menu",
			componentProps: { data: p__producto }
		});

		popover.onDidDismiss()
			.then(async (result) => {
				console.log(result);

				this.v__listaProductosVender[p_index].cantidad = result.data;
				this.calcularTotal();

				const toast = await this.toastController.create({
					message: `Editado ${p__producto.producto.descripcion} = ${result.data}`,
					duration: 2000,
					buttons: [
						{
							text: "Okay",
							role: "cancel",
							handler: () => {
								toast.dismiss();
							}
						}
					]
				});
				toast.present();

			});

		return await popover.present();
	}

	calcularTotal() {
		this.v__totalVenta = 0;
		this.v__listaProductosVender.forEach((p__item, p_index) => {// console.log(p__item);
			const c__total = p__item.producto.precio * p__item.cantidad;
			this.v__listaProductosVender[p_index].totalItem = c__total;
			this.v__totalVenta += c__total;
			this.ng__Efectivo = this.v__totalVenta;
			this.v__Efectivo = this.ng__Efectivo;
		});
	}

	async metodosPago(p__event) {
		if (p__event.target.id === "transferencia") {
			if (
				this.ng__Transferencia > this.v__totalVenta ||
				this.ng__Transferencia < 0
			) {
				const toast = await this.toastController.create({
					message: `la transferencia no puede ser mayor a la venta o menor a cero`,
					duration: 2000,
					color: "danger",
					buttons: [
						{
							text: "Okay",
							role: "cancel",
							handler: () => {
								toast.dismiss();
							}
						}
					]
				});
				toast.present();
				this.ng__Transferencia = 0;
			}
			this.ng__Efectivo = this.v__Efectivo - this.ng__Transferencia;
		} else {
			if (this.ng__Efectivo < this.v__totalVenta) {
				this.ng__Efectivo = this.v__Efectivo;
			} else {
				this.v__Efectivo = this.ng__Efectivo;
				this.ng__Transferencia = 0;
			}
		}
	}

	async registrarVenta() {
		let id = this.v__idFactura.split("00");

		console.log(id);

		const c__venta: i_Ventas = {
			id: `00${Number(id[1]) + 1}`,
			cliente: this.v__clienteSeleccionado,
			productos: this.v__listaProductosVender,
			creacion: new Date(),
			anulacion: null,
			totalVenta: this.v__totalVenta,
			saldo: 0,
			cambio: this.v__Cambio,
			debe: this.v__Debe,
			estado: "ACTIVO",
			metodoPago: {
				efectivo: this.ng__Efectivo,
				transferencia: this.ng__Transferencia
			}
		};
		await this.cr__service.addVenta(c__venta).then(res => {
			console.log(res);
		});
		this.resetForm();
		const toast = await this.toastController.create({
			message: `Venta registrada correctamente`,
			duration: 2000,
			buttons: [
				{
					text: "Okay",
					role: "cancel",
					handler: () => {
						toast.dismiss();
					}
				}
			]
		});
		toast.present();


	}

	resetForm() {
		this.v__listaProductosVender = [];

		this.v__cantidad = 0;
		this.v__precio = 0;

		this.v__descuentoPorcentaje = 0;
		this.v__descuentoValor = 0;
		this.v__valorEfectivo = 0;
		this.v__valorTransferencia = 0;

		this.v__totalVenta = 0;
		this.v__totalPagar = 0;

		this.initFilterClientes();

		this.ng__Efectivo = 0;
		this.v__Efectivo = 0;
		this.ng__Transferencia = 0;
		this.v__Cambio = 0;
		this.v__Debe = 0;
	}



	async showToast(p__type: string, p__message) {

		let color;

		if (p__type == 'error') {
			color = 'red';
		} else {
			color = 'white';
		}

		const toast = await this.toastController.create({
			message: p__message,
			duration: 5000,
			color: color,
			keyboardClose: true,
			buttons: [
				{
					text: "Okay",
					role: "cancel",
					handler: () => {
						toast.dismiss();
					}
				}
			]
		});
		toast.present();
	}

	//This will list all of your bluetooth devices
	async listPrinter() {
		try {
			this.bluetoothList = await this.print.searchBluetoothPrinter();
			console.log(this.bluetoothList);

		} catch (error) {
			console.log(error)
		}
	}

	//This will print
	async printStuff() {
		//The text that you want to print
		console.log(this.v__listaProductosVender);
		
		this.print.print(this.selectedPrinter, this.v__listaProductosVender);
	}
}