import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-menu-sales',
  templateUrl: './menu-sales.component.html',
  styleUrls: ['./menu-sales.component.scss'],
})
export class MenuSalesComponent implements OnInit {

  v__cantidad: number = 0;

  constructor(private navParams: NavParams, private pop: PopoverController) { 
    let value1 = navParams.get('data');
    console.log(value1);
    this.v__cantidad = value1.cantidad;
  }
    

  ngOnInit() { }

  async close() {
    await this.pop.dismiss(Number(this.v__cantidad))
  }

}
