import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";

import { HomePage } from "./home.page";
import { AngularMaterialModule } from "../angular-material/angular-material.module";
import { HomeComponent } from "./components/home/home.component";
import { SalesComponent } from "./components/sales/sales.component";
import { UserComponent } from "./components/user/user.component";
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { PrintService } from 'src/services/print/print.service';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        AngularMaterialModule,
        RouterModule.forChild([
            {
                path: "",
                component: HomePage,
                children: [
                    {
                        path: "home",
                        component: HomeComponent,
                        data: {
                            title: "Inicio",
                            showHeader: true
                        }
                    },
                    {
                        path: "sales",
                        component: SalesComponent,
                        data: {
                            title: "Ventas",
                            showHeader: true
                        }
                    },
                    {
                        path: "user",
                        component: UserComponent,
                        data: {
                            title: "Usuario",
                            showHeader: false
                        }
                    }
                ]
            }
        ])
    ],
    declarations: [HomePage, HomeComponent, SalesComponent, UserComponent],
    providers: [HomeComponent, SalesComponent, UserComponent, BluetoothSerial,
        PrintService],
})
export class HomePageModule { }
