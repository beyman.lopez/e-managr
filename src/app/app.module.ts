import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { MenuAppComponent } from "./menu/menu-app/menu-app.component";

import { AngularMaterialModule } from "./angular-material/angular-material.module";
import {
    BrowserAnimationsModule,
    NoopAnimationsModule
} from "@angular/platform-browser/animations";
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore'
import { MenuSalesComponent } from './home/components/sales/menu-sales/menu-sales/menu-sales.component';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';
import { Utils } from './utils/utils';

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////   APP ANIMATIONS   ////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

/*HABILITAR O DESHABILITAR LAS ANIMACIONES DE ANGULAR MATERIAL ( AngularMaterialModule )
CON UNA VARIABLE EN EL localStorage*/

if (!localStorage.getItem("app_animations")) {
    localStorage.setItem("app_animations", "enabled");
}

const animations: string = localStorage.getItem("app_animations");

var AnimationsModule: any =
    animations == "disabled" ? NoopAnimationsModule : BrowserAnimationsModule;

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////   APP ANIMATIONS   ////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

@NgModule({
    declarations: [AppComponent, MenuAppComponent, MenuSalesComponent,Utils],
    entryComponents: [MenuAppComponent, MenuSalesComponent],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        AnimationsModule /* ANGULAR MATERIAL ANIMATIONS (BrowserAnimationsModule),(NoopAnimationsModule) */,
        AngularMaterialModule,
        BrowserAnimationsModule /* ANGULAR MATERIAL COMPONENTS IMPORT*/,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
        AngularFireAuthModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        AndroidPermissions,
        AngularFireAuth,
        Utils
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
