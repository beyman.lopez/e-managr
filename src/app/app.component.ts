import { Component } from "@angular/core";

import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Platform } from "@ionic/angular";
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
    selector: "app-root",
    templateUrl: "app.component.html",
    styleUrls: ["app.component.scss"]
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private androidPermissions: AndroidPermissions
    ) {
        this.initializeApp();
    }

    initializeApp() {
        const prefersDark = window.matchMedia("(prefers-color-scheme: dark)");

        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });

        if (localStorage.getItem("darkTheme") === "true") {
            localStorage.setItem("darkTheme", "true");
            document.body.classList.add("dark");
        } else {
            localStorage.setItem("darkTheme", "false");
            document.body.classList.remove("dark");
        }

        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BLUETOOTH).then(
            result => console.log('Has permission?', result.hasPermission),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BLUETOOTH)
        );
    }
}
