import { Component, OnInit } from "@angular/core";
import {
    PopoverController,
    NavController,
    ToastController
} from "@ionic/angular";
import { MenuAppComponent } from "../menu/menu-app/menu-app.component";
import { AuthService } from 'src/services/auth/auth.service';

@Component({
    selector: "app-auth",
    templateUrl: "./auth.page.html",
    styleUrls: ["./auth.page.scss"]
})
export class AuthPage implements OnInit {
    email: string = "";
    password: string = "";
    v__login: boolean = true;
    constructor(
        public popoverController: PopoverController,
        public nav: NavController,
        public toastController: ToastController,
        private auth: AuthService,
    ) { }

    ngOnInit() { }

    async presentPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: MenuAppComponent,
            event: ev,
            translucent: true,
            cssClass: "app-menu"
        });

        return await popover.present();
    }

    async Login1() {
        if (this.email === "" || this.password === "") {
            
        } else {
            this.v__login = false;
            if (this.email === "admin" && this.password === "sanrafael") {
                setTimeout(() => {
                    this.nav.navigateRoot("home/home");
                }, 1000);
            } else {
                setTimeout(async () => {
                    const toast = await this.toastController.create({
                        message: `Nombre de usuario o contraseña incorrectos`,
                        duration: 2000,
                        buttons: [
                            {
                                text: "Okay",
                                role: "cancel",
                                handler: () => {
                                    toast.dismiss();
                                }
                            }
                        ]
                    });
                    toast.present();
                    this.v__login = true;
                }, 1000);
            }
        }
    }

    async Login() {
        let user = {
            email: this.email,
            password: this.password
        }
        const data = await this.auth.onLogin(user);
        if (data) {
            this.nav.navigateRoot("home/home");
        }
        console.log(user);
        console.log(data);
    }
}
