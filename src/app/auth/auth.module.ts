import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthPageRoutingModule } from './auth-routing.module';

import { AuthPage } from './auth.page';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { HomeComponent } from '../home/components/home/home.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuthPageRoutingModule,
    AngularMaterialModule
  ],
  declarations: [AuthPage],
  providers: [HomeComponent]
})
export class AuthPageModule {}
