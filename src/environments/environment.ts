// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC8Gq-6OwaLAtrADQHl6fOaC3pdiakrrT8",
    authDomain: "e-managr.firebaseapp.com",
    databaseURL: "https://e-managr.firebaseio.com",
    projectId: "e-managr",
    storageBucket: "e-managr.appspot.com",
    messagingSenderId: "795939746914",
    appId: "1:795939746914:web:3bb0d0ed25f49048164d3b",
    measurementId: "G-9TV3BKLC9P"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
