export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyC8Gq-6OwaLAtrADQHl6fOaC3pdiakrrT8",
    authDomain: "e-managr.firebaseapp.com",
    databaseURL: "https://e-managr.firebaseio.com",
    projectId: "e-managr",
    storageBucket: "e-managr.appspot.com",
    messagingSenderId: "795939746914",
    appId: "1:795939746914:web:3bb0d0ed25f49048164d3b",
    measurementId: "G-9TV3BKLC9P"
  }
};
