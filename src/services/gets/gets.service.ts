import { Injectable } from "@angular/core";
import {
    AngularFirestoreCollection,
    AngularFirestore
} from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { i_Productos, i_Clientes, i_Ventas } from "../../models/interfaces";
import { database } from 'firebase';

@Injectable({
    providedIn: "root"
})
export class GetsService {
    private productosCollection: AngularFirestoreCollection<i_Productos>;
    private v__productos: Observable<i_Productos[]>;

    private clientesCollection: AngularFirestoreCollection<i_Clientes>;
    private v__clientes: Observable<i_Clientes[]>;

    private ventasCollection: AngularFirestoreCollection<i_Ventas>;
    private v__ventas: Observable<i_Ventas[]>;

    constructor(public Database: AngularFirestore) {
        this.productosCollection = Database.collection<i_Productos>(
            "productos"
        );
        this.v__productos = this.productosCollection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(p__a => {
                    const data = p__a.payload.doc.data();
                    const id = p__a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );

        this.clientesCollection = Database.collection<i_Clientes>("clientes");
        this.v__clientes = this.clientesCollection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(p__a => {
                    const data = p__a.payload.doc.data();
                    const id = p__a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );

        this.ventasCollection = Database.collection<i_Ventas>("ventas");
        this.v__ventas = this.ventasCollection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(p__a => {
                    const data = p__a.payload.doc.data();
                    const id = p__a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );
    }

    obtenerProductos() {
        return this.v__productos;
    }

    obtenerClientes() {
        return this.v__clientes;
    }

    obtenerVentas() {
        return this.v__ventas;
    }

    // obtenerVenta(id: string) {
    //     this.todoCollection.doc<TaskI>(id).valueChanges();
    // }

    addVenta(p__venta: i_Ventas) {
        console.log("entro al metodo");

        var alovelaceDocumentRef = this.Database.doc('productos/001');


        console.log(alovelaceDocumentRef);
        
        return this.ventasCollection.add(p__venta);
    }

}
