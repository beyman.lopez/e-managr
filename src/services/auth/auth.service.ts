import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Utils } from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLogged: any = false;

  constructor(public fireAuth: AngularFireAuth, public util: Utils,) {
    fireAuth.authState.subscribe(user => (this.isLogged = user));
  }

  async onLogin(user) {
    try {
      return await this.fireAuth.auth.signInWithEmailAndPassword(
        user.email,
        user.password
      );
    } catch (error) {
      console.log(error);
      if (error.code == "auth/invalid-email") {
        this.util.openToast('e', 'Ingrese un correo valido');
      }
      if (error.code == "auth/wrong-password") {
        this.util.openToast('e', 'Contraseña incorrecta');
      }
      if (error.code == "auth/user-not-found") {
        this.util.openToast('e', 'Este usuario no está registrado');
      }
    }
  }

  async onRegister(user) {
    try {
      return await this.fireAuth.auth.createUserWithEmailAndPassword(
        user.email,
        user.password
      );
    } catch (error) {
      console.log(error);
    }
  }

  async onLogOut() {
    try {
      return await this.fireAuth.auth.signOut();
    } catch (error) {
      console.log(error);
    }
  }

}
