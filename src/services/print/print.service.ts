import { Injectable } from '@angular/core';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import EscPosEncoder from 'esc-pos-encoder-ionic';

@Injectable({
	providedIn: 'root'
})
export class PrintService {

	constructor(private bluetoothSerial: BluetoothSerial) { }

	//This will print
	async print(selectedPrinter, __prods) {

		let status = await this.__print(selectedPrinter, this.imprimirCabecera(), __prods);
		console.log(status);
		console.log(__prods);
	}

	__print(macAddress, data_string, __prods) {
		//1. Try connecting to bluetooth printer
		console.log(data_string);

		return new Promise((resolve, reject) => {
			this.connectToBluetoothPrinter(macAddress)
				.subscribe(_ => {
					//2. Connected successfully
					this.bluetoothSerial.write(data_string)
						.then(async _ => {
							//3. Print successful
							//If you want to tell user print is successful,
							//handle it here
							//4. IMPORTANT! Disconnect bluetooth after printing
							let cabecera = [];
							//6, | , 12, | , 7, | , 16
							// cabecera.push(`42 CARACTERES SMALL`);
							cabecera.push(`--------------------------------`);
							cabecera.push(`REF...*DESCRIPCION *CANT...*$ PRECIO`);
							cabecera.push(`--------------------------------`);
							// cabecera.push(`32 CARACTERES NORMAL`);
							console.log(cabecera);
							let status1 = await this.sendToBluetoothPrinter(this.imprimirProductos(cabecera, true), false);
							__prods.forEach(async (__producto, index) => {
								let v__listaProductosVender = []
								let id = await this.agregarEspacios(6, __producto.producto.id.toString());
								let descripcion = await this.agregarEspacios(12, __producto.producto.descripcion.toString());
								let cantidad = await this.agregarEspacios(7-1, __producto.cantidad.toString());
								let precio = await this.agregarEspacios(16, __producto.totalItem.toString());
								v__listaProductosVender.push(`${id.toUpperCase()}*${descripcion.toUpperCase()}* ${cantidad}*${Number(precio).toLocaleString('de-DE')}`);
								console.log(v__listaProductosVender);
								v__listaProductosVender.push(`--------------------------------`);
								let status1 = await this.sendToBluetoothPrinter(this.imprimirProductos(v__listaProductosVender, false), false);
								console.log(status1);
								if (index == __prods.length - 1) {
									let status2 = await this.sendToBluetoothPrinter(this.imprimirFooter(), true);
									console.log(status2);
								}
							});
							setTimeout(() => {
								resolve('We finished mowing the lawn')
							}, 5000);
						}, err => {
							//If there is an error printing to bluetooth printer
							//handle it here
							reject(err)
						})
				}, err => {
					reject(err)

					//If there is an error connecting to bluetooth printer
					//handle it here
				})
		})

	}


	sendToBluetoothPrinter(data_string, disconect) {
		//1. Try connecting to bluetooth printer
		console.log(data_string);

		return new Promise((resolve, reject) => {
			this.bluetoothSerial.write(data_string)
				.then(_ => {
					//3. Print successful
					//If you want to tell user print is successful,
					//handle it here
					//4. IMPORTANT! Disconnect bluetooth after printing
					if (disconect) {
						this.disconnectBluetoothPrinter()
					}
					// this.bluetoothSerial.isConnected()
					resolve('We finished mowing the lawn')
				}, err => {
					//If there is an error printing to bluetooth printer
					//handle it here
					reject(err)
				})
		})

	}

	imprimirCabecera(): any {
		const encoder = new EscPosEncoder();
		return encoder
			.initialize()
			.align('center')
			.size('normal')
			.line('--------------------------------')
			.bold()
			.line('FACTURA DE VENTA')
			.line('PRODUCTOS DEL CAMPO SAN RAFAEL')
			.bold(false)
			.line('--------------------------------')
			.align('left')
			.size('small')
			.line('NIT            | 0011162365600656')
			.line('DIRECCION      | Calle 5 # 12-120')
			.line('TEL            | 3350000')
			.size('normal')
			.line('--------------------------------')
			.size('small')
			.line('FACTURA DE VENTA No. 001')
			.line('FECHA          | 02 | 08 | 2020')
			.line('CLIENTE        | PEPITO PEREZ')
			.line('IDENTIFICACION | 11440552255')
			.align('center')
			.size('normal')
			.line('--------------------------------')
			.bold()
			.line('LISTA DE PRODUCTOS')
			.bold(false)
			.line('--------------------------------')
			.encode();
	}

	imprimirProductos(value: any, cabecera: boolean): any {
		const encoder = new EscPosEncoder();
		if (cabecera) {
			return encoder
				.initialize()
				.align('left')
				.size('normal')
				.line(value[0])
				.size('small')
				.bold()
				.line(value[1])
				.bold(false)
				.size('normal')
				.line(value[2])
				.encode();
		} else {
			return encoder
				.initialize()
				.align('left')
				.size('small')
				.line(value[0])
				.size('normal')
				.line(value[1])
				.encode();
		}
	}

	imprimirFooter(): any {
		const encoder = new EscPosEncoder();
		return encoder
			.initialize()
			.align('center')
			.size('normal')
			.bold()
			.line('GRACIAS POR SU COMPRA')
			.bold(false)
			.line('--------------------------------')
			.align('left')
			.line('')
			.line('')
			.cut()
			.encode();
	}

	agregarEspacios(lengthVar: number, Texto: string) {// console.log(str.length);

		let res = '';

		if (Texto.length <= lengthVar - 3) {
			let dots = '   '//'   ' = '...'
			if (Texto.length < lengthVar - 3) {
				dots = dots.substring(0, lengthVar - Texto.length) + '   ';//'   ' = '...'
			}
			Texto = Texto + dots
			Texto += new Array(lengthVar + 1).join(' ');//' ' = '.'

			res = Texto.substring(0, lengthVar);

		} else if (Texto.length > lengthVar - 3) {

			if (Texto.length == length) {
				Texto = Texto.substring(0, lengthVar);
			} else if (Texto.length == lengthVar - 1) {
				Texto = Texto.substring(0, lengthVar);
				Texto = Texto + ' '//' ' = '.'
			} else if (Texto.length == lengthVar - 2) {
				Texto = Texto.substring(0, lengthVar);
				Texto = Texto + '  '//'  ' = '..'
			} else if (Texto.length == lengthVar - 3) {
				Texto = Texto.substring(0, lengthVar);
				Texto = Texto + '   '//'   ' = '...'
			} else {
				Texto = Texto.substring(0, lengthVar - 3);
				Texto = Texto + '...'
			}

			res = Texto;

		}// console.log(res);

		return res;

	}

	searchBluetoothPrinter() {
		//This will return a list of bluetooth devices
		this.bluetoothSerial.enable();
		return this.bluetoothSerial.list();
	}

	connectToBluetoothPrinter(macAddress) {
		//This will connect to bluetooth printer via the mac address provided
		return this.bluetoothSerial.connect(macAddress)
	}

	disconnectBluetoothPrinter() {
		//This will disconnect the current bluetooth connection
		return this.bluetoothSerial.disconnect();
	}

}
